@if(session('error'))
    <div class="alert alert-danger alert-message"><span class="alert-text">{!! session('error') !!}</span></div>
@endif
@if(session('danger'))
    <div class="alert alert-danger alert-message"><span class="alert-text">{!! session('danger') !!}</span></div>
@endif
@if(session('warning'))
    <div class="alert alert-warning alert-message"><span class="alert-text">{!! session('warning') !!}</span></div>
@endif
@if(session('info'))
    <div class="alert alert-info alert-message"><span class="alert-text">{!! session('info') !!}</span></div>
@endif
@if(session('primary'))
    <div class="alert alert-primary alert-message"><span class="alert-text">{!! session('primary') !!}</span></div>
@endif
@if(session('secondary'))
    <div class="alert alert-secondary alert-message"><span class="alert-text">{!! session('secondary') !!}</span></div>
@endif
@if(session('success'))
    <div class="alert alert-success">{!! session('success') !!}</div>
@endif
