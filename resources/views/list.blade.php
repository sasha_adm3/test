@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">List</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Тема</th>
                            <th>Сообщение</th>
                            <th>Имя клиента</th>
                            <th>Почта клиента</th>
                            <th>Файл</th>
                            <th>Создано</th>
                            <th>Ответил</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->subject}}</td>
                                <td>{{$item->message}}</td>
                                <td>{{$item->user->name}}</td>
                                <td>{{$item->user->email}}</td>
                                <td>
                                    {!! ($item->file ? '<a href="'.route('fileDownload', ['message_id' => $item->id]).'">Скачать файл</a>' : '-') !!}
                                </td>
                                <td>{{$item->created_at->format('d.m.Y H:i')}}</td>
                                <td>
                                    <input class="check_message"
                                           type="checkbox"
                                           data-id="{{$item->id}}"
                                           data-url="{{route('checkMessage')}}"
                                           {!! ($item->check ? 'checked' : '') !!}
                                    >
                                    <p class="result" style="display: none">132</p>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="box-footer"><div class="text-center">{{ $messages->links() }}</div></div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
