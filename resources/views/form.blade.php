@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Form</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('submitForm') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="subject" class="col-md-4 col-form-label text-md-right">Тема</label>

                            <div class="col-md-6">
                                <input id="subject" type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" value="{{ old('subject') }}" required>

                                @error('subject')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-md-4 col-form-label text-md-right">Сообщение</label>

                            <div class="col-md-6">
                                <textarea name="message" id="message" class="form-control @error('message') is-invalid @enderror" required></textarea>
                                @error('message')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">Файл</label>

                            <div class="col-md-6">
                                <input id="file" type="file" name="file" class="@error('file') is-invalid @enderror">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Отправить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
