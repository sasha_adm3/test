$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.check_message').on('click', function () {
        var $this = $(this),
            url = $this.data('url'),
            message_id = $this.data('id'),
            value = 0;

        if($this[0].checked){
            value = 1;
        }

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                message_id: message_id,
                value: value
            }
        });

    });
});
