<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => 'can:manager'], function () {
        Route::get('/manager-list', 'ManagerController@list')->name('message.list');

        Route::get('download-file/{message_id}', 'ManagerController@fileDownload')->name('fileDownload');
        Route::post('check-message', 'ManagerController@checkMessage')->name('checkMessage');
    });

    Route::get('/', 'ClientController@index')->name('client.form');

    Route::post('/submit-form', 'ClientController@submitForm')->name('submitForm');
});


