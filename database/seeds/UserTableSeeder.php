<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_manager  = Role::where('name', 'manager')->first();

        $admin = new User();
        $admin->name = 'Manager';
        $admin->email = 'manager@test.com';
        $admin->password = bcrypt('password');
        $admin->save();
        $admin->roles()->attach($role_manager);
    }
}
