<?php

namespace App;

use App\Mail\SendMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;


class Message extends Model
{
    protected $fillable = ['user_id', 'subject', 'message', 'file', 'check'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function sendManagerMail(){
        //поиск менеджера
        $manager = User::whereHas('roles', function ($q){
            $q->where('roles.name', 'manager');
        })->first();

        if($manager) {
            $user = $this->user;
            $data = $this->toArray();

            //отправка почты менеджеру
            Mail::to($manager->email)->send(new SendMessage($user, $data));
        }
    }
}
