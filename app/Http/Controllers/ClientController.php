<?php

namespace App\Http\Controllers;


use App\Message;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ClientController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index():View
    {
        //вывод формы обратной связи
        return view('form');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function submitForm(Request $request):RedirectResponse
    {
        $user = auth()->user();

        //проверка отправки формы пользователем(1 раз в 24 часа)
        if(!$user->validateDay()){
            session()->flash('error', 'Попробуйте позже');
            return redirect()->route('client.form');
        }

        $request->validate([
            'subject' => 'required|max:255',
            'message' => 'required',
            'file' => 'nullable|max:100',
        ]);

        $form = $request->all();

        //сохранение прикрепленного файла
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $form['file'] = $request->file->store('public/file');
        }

        $form['user_id'] = $user->id;

        //сохранение сообщения в бд
        $message = new Message($form);
        $message->save();

        //отправка сообщения на почту менеджеру
        $message->sendManagerMail();

        session()->flash('success', 'Сообщение отправлено');

        return redirect()->route('client.form');
    }
}
