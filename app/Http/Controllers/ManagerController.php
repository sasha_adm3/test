<?php

namespace App\Http\Controllers;


use App\Message;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ManagerController extends Controller
{
    /**
     * @return Factory|View
     */
    public function list():View
    {
        //вывод всех сообщений с пагинацией
        $messages = Message::paginate();

        return view('list', compact('messages'));
    }


    /**
     * @param int $message_id
     * @return mixed|RedirectResponse
     */
    public function fileDownload(int $message_id){
        //поиск сообщения с файлом
        $message = Message::findOrFail($message_id);

        //проверка на существование файла, если найден отдать на скачивание
        if(Storage::exists($message->file)){
            return Storage::download($message->file);
        }

        //если не найден, возврат с ошибкой
        session()->flash('error', 'Файл не найден');

        return redirect()->back();
    }


    public function checkMessage(Request $request){
        $request->validate([
            'message_id' => 'required|integer',
            'value' => 'required|integer',
        ]);

        $message_id = $request->message_id;
        $value = $request->value;

        //поиск сообщения с файлом
        $message = Message::findOrFail($message_id);

        //изменение значения
        $message->check = $value;
        $message->save();
    }
}
