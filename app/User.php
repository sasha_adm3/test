<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function roles(){
        return $this->belongsToMany(Role::class, 'user_roles');
    }


    protected $roleLevels = [
        'manager' => 100
    ];


    public function hasRole($role){
        if($this->roles->count()){
            foreach ($this->roles as $v){
                if($v->name == $role){
                    return true;
                }
            }
        }
        return false;
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function getLastMessageAttribute(){
        return $this->messages()->orderBy('created_at', 'desc')->first();
    }


    public function validateDay(){
        if($this->lastMessage){
            $now = Carbon::now();
            $lastMessageDate = Carbon::parse($this->lastMessage->created_at);

            $diffInHours = $now->diffInHours($lastMessageDate);

            if($diffInHours <= 24){
                return false;
            }

        }
        return true;
    }
}
